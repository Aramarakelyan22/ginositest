 export const data = [
  {
    "id": "16771079",
    "name": "Lombardi's Pizza",
    "location": {
      "address": "32 Spring Street, New York 10012",
      "longitude": "-73.9955888889",
      "latitude": "40.7216750000"
    },
    "user_rating": "4.8",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16771079_RESTAURANT_da60c9abb32fa64cddc148a2795ae43c_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16762160",
    "name": "Carmine's Italian Restaurant",
    "location": {
      "address": "200 West 44th Street, btwn Broadway & 8th Avenue 10036",
      "longitude": "-73.9875700000",
      "latitude": "40.7579000000"
    },
    "user_rating": "4.7",
    "thumb": ""
  },
  {
    "id": "16783998",
    "name": "The Halal Guys",
    "location": {
      "address": "6th Avenue & W 53rd Street, New York 10019",
      "longitude": "-73.9790839000",
      "latitude": "40.7617884000"
    },
    "user_rating": "4.9",
    "thumb": ""
  },
  {
    "id": "16764005",
    "name": "Daniel",
    "location": {
      "address": "60 East 65th Street., Upper East Side 10065",
      "longitude": "-73.9679500000",
      "latitude": "40.7667900000"
    },
    "user_rating": "4.1",
    "thumb": ""
  },
  {
    "id": "16761344",
    "name": "Buddakan",
    "location": {
      "address": "75 9th Avenue, New York 10011",
      "longitude": "-74.0055112000",
      "latitude": "40.7422320000"
    },
    "user_rating": "4.9",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16761344_RESTAURANT_09252a25cb35a75a4cb8dde2eef68870.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16771928",
    "name": "Max Brenner",
    "location": {
      "address": "841 Broadway, New York 10003",
      "longitude": "-73.9914333333",
      "latitude": "40.7344138889"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16771928_RESTAURANT_7d9c8d906a8ab2130214b424f415e514_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16781904",
    "name": "Momofuku Noodle Bar",
    "location": {
      "address": "171 1st Avenue 10003",
      "longitude": "-73.9843200000",
      "latitude": "40.7291000000"
    },
    "user_rating": "4.7",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16781904_RESTAURANT_fe3097ac80f3396f383f8ba588bcf832_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16777384",
    "name": "Shake Shack",
    "location": {
      "address": "Madison Square Park, 23rd & Madison, New York 10010",
      "longitude": "-73.9879841000",
      "latitude": "40.7408681000"
    },
    "user_rating": "4.9",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16777384_CHAIN_10d8440cfa6b530875c2cc6067e31349_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16782050",
    "name": "5 Napkin Burger",
    "location": {
      "address": "630 9th Avenue, Hell'South Kitchen 10036",
      "longitude": "-73.9913800000",
      "latitude": "40.7599800000"
    },
    "user_rating": "4.8",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16782050_RESTAURANT_4377a36b355bbd1c3696869dab8c5a3a_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16769241",
    "name": "Junior's Restaurant",
    "location": {
      "address": "1515 Broadway--Between Broadway and 8th Avenue, Midtown - Times Square 10019",
      "longitude": "-73.9864000000",
      "latitude": "40.7579300000"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16769241_RESTAURANT_9441b6ae1f582f44f29a61d11052d476_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16777320",
    "name": "Serendipity 3",
    "location": {
      "address": "225 E 60th Street, New York 10022",
      "longitude": "-73.9648580000",
      "latitude": "40.7618990000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16777320_RESTAURANT_5bc92fcf35438e2404eb918270b2ea2c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16781875",
    "name": "Ippudo",
    "location": {
      "address": "65 4th Avenue, New York 10003",
      "longitude": "-73.9906833333",
      "latitude": "40.7307361111"
    },
    "user_rating": "4.8",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16781875_RESTAURANT_d0f7a0565b23d07be15d3e8326d41066.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16760100",
    "name": "Balthazar",
    "location": {
      "address": "80 Spring Street 10012",
      "longitude": "-73.9980500000",
      "latitude": "40.7225900000"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16760100_RESTAURANT_32746fc377c99a018337c4d3b7c1ce2a.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16759908",
    "name": "Babbo",
    "location": {
      "address": "110 Waverly Pl, Greenwich Village 10011",
      "longitude": "-73.9992800000",
      "latitude": "40.7322800000"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16759908_RESTAURANT_624e0ab469f76b114b4a08f576b43306_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16763244",
    "name": "Clinton Street Baking Company",
    "location": {
      "address": "4 Clinton Street, New York 10002",
      "longitude": "-73.9840694444",
      "latitude": "40.7212305556"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16763244_RESTAURANT_48a8076f9dd4704a14e338dc373fa8cb_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16765367",
    "name": "Eleven Madison Park",
    "location": {
      "address": "11 Madison Avenue, New York 10010",
      "longitude": "-73.9866285000",
      "latitude": "40.7415086000"
    },
    "user_rating": "4.7",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16765367_RESTAURANT_bd2d075a982733fa099b3f00c43e30e1_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16769089",
    "name": "John's Pizza of Times Square",
    "location": {
      "address": "260 W 44th Street, New York 10036",
      "longitude": "-73.9882666667",
      "latitude": "40.7584111111"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16769089_RESTAURANT_a7a23ba46bdbe76f7ffffc175679ede9_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16779248",
    "name": "Tao",
    "location": {
      "address": "42 E 58th Street, New York 10022",
      "longitude": "-73.9713750000",
      "latitude": "40.7627111111"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16779248_RESTAURANT_68d0ceba78997b23ff011573ee029f13_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16775039",
    "name": "Peter Luger Steak House",
    "location": {
      "address": "178 Broadway, Brooklyn 11211",
      "longitude": "-73.9623416667",
      "latitude": "40.7098777778"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16775039_RESTAURANT_18bb213a181da7fe5479eeefe2adfa66.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16767139",
    "name": "Gramercy Tavern",
    "location": {
      "address": "42 E 20th Street 10003",
      "longitude": "-73.9883888889",
      "latitude": "40.7387166667"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16767139_RESTAURANT_cbb816b562e110e37786151d6ea47009.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16770626",
    "name": "Le Bernardin",
    "location": {
      "address": "155 W 51st Street, New York 10019",
      "longitude": "-73.9819194444",
      "latitude": "40.7608027778"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16770626_RESTAURANT_73f09e4b6f5cc7267cae1c5804f1574c_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16780467",
    "name": "Veselka",
    "location": {
      "address": "144 2nd Avenue 10003",
      "longitude": "-73.9870800000",
      "latitude": "40.7287300000"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16780467_RESTAURANT_6358ddba766cd068451b21e01fa4a6f4_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16761402",
    "name": "Burger Joint",
    "location": {
      "address": "Le Parker Meridien Hotel, 119 W 56th Street, New York 10019",
      "longitude": "-73.9784051000",
      "latitude": "40.7641849000"
    },
    "user_rating": "4.7",
    "thumb": ""
  },
  {
    "id": "16775526",
    "name": "Pommes Frites",
    "location": {
      "address": "128 Macdougal Street, New York 10012",
      "longitude": "-74.0001620000",
      "latitude": "40.7300310000"
    },
    "user_rating": "4.7",
    "thumb": ""
  },
  {
    "id": "16776778",
    "name": "S'MAC",
    "location": {
      "address": "197 First Avenue, New York 10003",
      "longitude": "-73.9840388889",
      "latitude": "40.7303694444"
    },
    "user_rating": "4.8",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16776778_RESTAURANT_249bb1c706c51acfa5b933b9becba5ac_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16774980",
    "name": "Per Se",
    "location": {
      "address": "Time Warner Center, 10 Columbus Circle, 4th Floor, New York 10019",
      "longitude": "-73.9827037000",
      "latitude": "40.7684141000"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16774980_RESTAURANT_b010c67d47227f27aa142dfd13adaa3e_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16769041",
    "name": "Joe's Shanghai",
    "location": {
      "address": "9 Pell Street 10013",
      "longitude": "-73.9977800000",
      "latitude": "40.7146000000"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16769041_RESTAURANT_f2223a1bf1b16545e39797137d9a4c97_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16767332",
    "name": "Grimaldi's Pizzeria",
    "location": {
      "address": "1 Front Street, Brooklyn 11201",
      "longitude": "-73.9932722222",
      "latitude": "40.7025138889"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16767332_RESTAURANT_3e45ed0b49952f5dd0f7ba4c98b9ab2c_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16776567",
    "name": "Rosa Mexicano",
    "location": {
      "address": "61 Columbus Avenue 10023",
      "longitude": "-73.9833600000",
      "latitude": "40.7711200000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16776567_RESTAURANT_75347e8115427b9829045a51b2f4449f_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16760901",
    "name": "Blue Smoke",
    "location": {
      "address": "116 E 27th Street, New York 10016",
      "longitude": "-73.9838080000",
      "latitude": "40.7422410000"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16760901_RESTAURANT_a025310993e538ec0cc59e2da80e3463_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16786585",
    "name": "Bagatelle",
    "location": {
      "address": "1 Little W 12th Street, New York 10014",
      "longitude": "-74.0062416667",
      "latitude": "40.7396000000"
    },
    "user_rating": "4.1",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16786585_RESTAURANT_00551fbfad1ad4af7522759f4bb13b0f_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16786233",
    "name": "Doughnut Plant",
    "location": {
      "address": "220 Between 7th & 8th Avenue, W 23rd Street, New York 10011",
      "longitude": "-73.9965666667",
      "latitude": "40.7446944444"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16764602_CHAIN_8e87ae769f497143a20018f69a8729d0_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16780641",
    "name": "Virgil's Real BBQ",
    "location": {
      "address": "152 West 44th Street, btwn Broadway & 6th Avenue 10036",
      "longitude": "-73.9851800000",
      "latitude": "40.7567400000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16780641_RESTAURANT_56f2a3936fdd097ba016a7521e6b3f08.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16764602",
    "name": "Doughnut Plant",
    "location": {
      "address": "379 Grand Street, New York 10002",
      "longitude": "-73.9885111111",
      "latitude": "40.7164500000"
    },
    "user_rating": "4.7",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16764602_CHAIN_8e87ae769f497143a20018f69a8729d0_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16761927",
    "name": "Café Habana",
    "location": {
      "address": "17 Prince Street, New York 10012",
      "longitude": "-73.9941930000",
      "latitude": "40.7229330000"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16761927_RESTAURANT_1e1f5ff368dae2c54f3cb2c34d490781_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16786034",
    "name": "Junoon",
    "location": {
      "address": "27 West 24th Street, Flatiron district 10010",
      "longitude": "-73.9908300000",
      "latitude": "40.7431800000"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16786034_RESTAURANT_82a355e62aa5d0486c7220390ce7133a_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16777961",
    "name": "The Spotted Pig",
    "location": {
      "address": "314 West 11th Street 10014",
      "longitude": "-74.0065000000",
      "latitude": "40.7355900000"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16777961_RESTAURANT_78c8dd0aaad6372b95d13f02291141d8_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16760367",
    "name": "Becco",
    "location": {
      "address": "355 West 46th Street, Theater District 10036",
      "longitude": "-73.9894300000",
      "latitude": "40.7606600000"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16760367_RESTAURANT_8d0ff25f51f5f31bd4e64085821420a6.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16782628",
    "name": "Momofuku Milk Bar",
    "location": {
      "address": "251 East 13th Street, New York 10003",
      "longitude": "-73.9858861111",
      "latitude": "40.7318805556"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16782628_RESTAURANT_80c997d8a1febfa55f2c23e31a2f255d_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16773829",
    "name": "Nobu",
    "location": {
      "address": "105 Hudson Street 10013",
      "longitude": "-74.0089900000",
      "latitude": "40.7195200000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16773829_RESTAURANT_352e37ab2d486fe64cced11df37f80cf.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16785398",
    "name": "Shake Shack",
    "location": {
      "address": "691 8th Avenue, New York 10036",
      "longitude": "-73.9890138889",
      "latitude": "40.7587361111"
    },
    "user_rating": "4.9",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16785398_RESTAURANT_9808b948d2739435ea95bd3002d95cda.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16772872",
    "name": "Morimoto",
    "location": {
      "address": "88 10th Avenue, New York 10011",
      "longitude": "-74.0072610000",
      "latitude": "40.7431250000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16772872_RESTAURANT_5dd5bf72fa87107cdfd98bf6e969e8ab.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16781877",
    "name": "Roberta's",
    "location": {
      "address": "261 Moore Street, Brooklyn 11206",
      "longitude": "-73.9343520000",
      "latitude": "40.7048930000"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16781877_CHAIN_9ef2af8621cdcb4f1026005aa04a72cd_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16770824",
    "name": "Levain Bakery",
    "location": {
      "address": "167 W 74th Street, New York 10023",
      "longitude": "-73.9797260000",
      "latitude": "40.7795120000"
    },
    "user_rating": "4.8",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16770824_RESTAURANT_d25bb734f25dd5484addc997f65e016c_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16769090",
    "name": "John's of Bleecker Street",
    "location": {
      "address": "278 Bleecker Street, New York 10014",
      "longitude": "-74.0032583333",
      "latitude": "40.7317500000"
    },
    "user_rating": "4.4",
    "thumb": ""
  },
  {
    "id": "16776730",
    "name": "Russ & Daughters",
    "location": {
      "address": "179 E Houston Street, New York 10002",
      "longitude": "-73.9882361111",
      "latitude": "40.7227361111"
    },
    "user_rating": "4.7",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16776730_RESTAURANT_d753c0f0816fa4e4eba4a958b0d1e414_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16783153",
    "name": "Shake Shack",
    "location": {
      "address": "366 Columbus Avenue W 77th Street, New York 10024",
      "longitude": "-73.9764944444",
      "latitude": "40.7807222222"
    },
    "user_rating": "4.9",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16777384_CHAIN_10d8440cfa6b530875c2cc6067e31349_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16784068",
    "name": "Magnolia Bakery",
    "location": {
      "address": "1240 Avenue of the Americas, New York 10020",
      "longitude": "-73.9804305556",
      "latitude": "40.7598305556"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16784068_RESTAURANT_f6d3bd9864d3bdcaf59d3055848dfe47_c.JPG?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16783477",
    "name": "Locanda Verde",
    "location": {
      "address": "377 Greenwich Street, New York 10013",
      "longitude": "-74.0100580000",
      "latitude": "40.7197680000"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16783477_RESTAURANT_f46a1a5f17acde739b4b8c5f2fa0e0e9_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16762096",
    "name": "Caracas Arepa Bar",
    "location": {
      "address": "93 East 7th Street, New York 10009",
      "longitude": "-73.9854138889",
      "latitude": "40.7268750000"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16762096_CHAIN_77e6b22932e4ffbdff4881348b69286e_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16768829",
    "name": "Jean-Georges",
    "location": {
      "address": "1 Central Park West 10023",
      "longitude": "-73.9815900000",
      "latitude": "40.7691200000"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16768829_RESTAURANT_c54a4a57a172204604a8ba6890958802_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16781900",
    "name": "Artichoke Basille's Pizza",
    "location": {
      "address": "328 East 14th Street, New York 10003",
      "longitude": "-73.9838333333",
      "latitude": "40.7317166667"
    },
    "user_rating": "4.4",
    "thumb": ""
  },
  {
    "id": "16784825",
    "name": "The Meatball Shop",
    "location": {
      "address": "84 Stanton Street, New York 10002",
      "longitude": "-73.9888083333",
      "latitude": "40.7216138889"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16784825_CHAIN_cdaf631b09b978f41c3250fb1dd8dd32_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16769568",
    "name": "Keens Steakhouse",
    "location": {
      "address": "72 W 36th Street, New York 10018",
      "longitude": "-73.9863805556",
      "latitude": "40.7508972222"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16769568_RESTAURANT_68db978d134c916c5351a39271fb39c6_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16781905",
    "name": "Ayza Wine & Chocolate Bar",
    "location": {
      "address": "11 West 31st Street, New York 10016",
      "longitude": "-73.9870388889",
      "latitude": "40.7470083333"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16781905_RESTAURANT_b22a0c0989f40210efb601e49508180c_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16783442",
    "name": "DBGB Kitchen & Bar",
    "location": {
      "address": "299 Bowery, New York 10003",
      "longitude": "-73.9923472222",
      "latitude": "40.7244500000"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16783442_RESTAURANT_de46d97d731e94f3d09fbbd72d99ddee.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16781606",
    "name": "Hill Country Barbecue Market",
    "location": {
      "address": "30 West 26th Street 10010",
      "longitude": "-73.9904800000",
      "latitude": "40.7441600000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16781606_RESTAURANT_bbe66bf9254607440029e03833db098a_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16761336",
    "name": "Bubby's",
    "location": {
      "address": "120 Hudson Street, New York 10013",
      "longitude": "-74.0084527778",
      "latitude": "40.7197444444"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16761336_CHAIN_15e9f48576e4f6365b3c8760217e11e4_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16767080",
    "name": "Gotham Bar and Grill",
    "location": {
      "address": "12 E 12th Street, New York 10003",
      "longitude": "-73.9935916667",
      "latitude": "40.7344027778"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16767080_RESTAURANT_0b718c5699d4ec9d9d4c64856c9d8b5a.jpeg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16761868",
    "name": "Cafeteria",
    "location": {
      "address": "119 7th Avenue 10011",
      "longitude": "-73.9980000000",
      "latitude": "40.7407100000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16761868_RESTAURANT_96b8a6ecbafc68cc3e4b3e4c1eb73266_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16772774",
    "name": "Momofuku Ssam Bar",
    "location": {
      "address": "207 2nd Avenue 10003",
      "longitude": "-73.9858500000",
      "latitude": "40.7316900000"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16772774_RESTAURANT_a9af82aa87b34e3bb752645d0e55b441_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16764344",
    "name": "Dinosaur Bar-B-Que",
    "location": {
      "address": "700 W 125th Street, New York 10027",
      "longitude": "-73.9607083333",
      "latitude": "40.8182972222"
    },
    "user_rating": "4.6",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16764344_RESTAURANT_b42281855f68171c8963571ea487e7ea_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16787297",
    "name": "Dominique Ansel Bakery",
    "location": {
      "address": "189 Spring Street, New York 10012",
      "longitude": "-74.0031138889",
      "latitude": "40.7250861111"
    },
    "user_rating": "4.4",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16787297_RESTAURANT_730e8f770468ab4ab8c1cff0887e2222_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16774318",
    "name": "Otto Enoteca Pizzeria",
    "location": {
      "address": "One Fifth Avenue at 8th Street, Greenwich Village 10003",
      "longitude": "-73.9965400000",
      "latitude": "40.7318200000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16774318_RESTAURANT_fc526e8cfdc1cd8242c50298385d325c.JPG?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16777242",
    "name": "Sea Restaurant",
    "location": {
      "address": "114 N 6th Street, Brooklyn 11249",
      "longitude": "-73.9598388889",
      "latitude": "40.7182055556"
    },
    "user_rating": "4.5",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16777242_RESTAURANT_8db1aeeda8b9d6c71adba7d5c695fdeb_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16761333",
    "name": "Bubba Gump Shrimp Co.",
    "location": {
      "address": "1501 Broadway, New York 10036",
      "longitude": "-73.9864400000",
      "latitude": "40.7570400000"
    },
    "user_rating": "4.1",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16761333_RESTAURANT_e0f5cab076cac9d4a736a1c13976d7c9_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16770236",
    "name": "La Esquina",
    "location": {
      "address": "114 Kenmare Street, New York 10012",
      "longitude": "-73.9978250000",
      "latitude": "40.7215000000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16770236_RESTAURANT_7b9d1c63602c982522d32144f2aa2806_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16764132",
    "name": "Del Posto",
    "location": {
      "address": "85 10th Avenue, Chelsea 10011",
      "longitude": "-74.0076300000",
      "latitude": "40.7430700000"
    },
    "user_rating": "4.1",
    "thumb": ""
  },
  {
    "id": "16783672",
    "name": "The Standard Grill",
    "location": {
      "address": "The Standard High Line, 848 Washington Street, New York 10014",
      "longitude": "-74.0078805556",
      "latitude": "40.7406388889"
    },
    "user_rating": "4.1",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16783672_RESTAURANT_217892c20bae89adefde68910363704e_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16771331",
    "name": "Lupa Osteria Romana",
    "location": {
      "address": "170 Thompson Street, New York 10012",
      "longitude": "-74.0002972222",
      "latitude": "40.7276833333"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16771331_RESTAURANT_7fad83ee405061ba127f7a67587179db_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16760892",
    "name": "Blue Ribbon Brasserie",
    "location": {
      "address": "97 Sullivan Street, New York 10012",
      "longitude": "-74.0031444444",
      "latitude": "40.7255250000"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16760892_RESTAURANT_4ac75dbb68178c33ef3259c221c72092_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16778226",
    "name": "Ellen's Stardust Diner",
    "location": {
      "address": "1650 Broadway, New York 10019",
      "longitude": "-73.9835444444",
      "latitude": "40.7616722222"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16778226_RESTAURANT_1626443adf8d0a12df86595189fce4b7_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16780256",
    "name": "Union Square Cafe",
    "location": {
      "address": "16th Street, New York 10003",
      "longitude": "-73.9913472222",
      "latitude": "40.7369333333"
    },
    "user_rating": "4.3",
    "thumb": ""
  },
  {
    "id": "16771494",
    "name": "Magnolia Bakery",
    "location": {
      "address": "401 Bleecker Street, New York 10014",
      "longitude": "-74.0050805556",
      "latitude": "40.7358694444"
    },
    "user_rating": "4.2",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16771494_RESTAURANT_eee7e63bac713aabfb5e8cdda15ab46e_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16780436",
    "name": "Veniero's",
    "location": {
      "address": "342 East 11th Street 10003",
      "longitude": "-73.9845400000",
      "latitude": "40.7293900000"
    },
    "user_rating": "4.3",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16780436_RESTAURANT_a631558a1d971662d70f61088748a13c_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16785219",
    "name": "Totto Ramen",
    "location": {
      "address": "366 W 52nd Street, New York 10019",
      "longitude": "-73.9875416667",
      "latitude": "40.7643888889"
    },
    "user_rating": "4.1",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16785219_RESTAURANT_db347406d311b93034d5b83ff933f164.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16767634",
    "name": "Hard Rock Cafe",
    "location": {
      "address": "1501 Broadway, New York 10036",
      "longitude": "-73.9864400000",
      "latitude": "40.7570400000"
    },
    "user_rating": "4.0",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16767634_RESTAURANT_a03a377373bf3b29b4e014d7d0423430_c.png?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16767193",
    "name": "Gray's Papaya",
    "location": {
      "address": "2090 Broadway, New York 10023",
      "longitude": "-73.9816277778",
      "latitude": "40.7785805556"
    },
    "user_rating": "4.4",
    "thumb": ""
  },
  {
    "id": "16763460",
    "name": "Cookshop",
    "location": {
      "address": "156 10th Avenue 10011",
      "longitude": "-74.0055400000",
      "latitude": "40.7456500000"
    },
    "user_rating": "4.1",
    "thumb": "https://b.zmtcdn.com/data/res_imagery/16763460_RESTAURANT_d0dab8c774ca2cc8fa421e67d818c8af_c.jpg?fit=around%7C200%3A200&crop=200%3A200%3B%2A%2C%2A"
  },
  {
    "id": "16784987",
    "name": "ABC Kitchen",
    "location": {
      "address": "35 E 18th Street, New York 10003",
      "longitude": "-73.9897222222",
      "latitude": "40.7376444444"
    },
    "user_rating": "4.5",
    "thumb": ""
  }
]