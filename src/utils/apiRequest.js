import 'whatwg-fetch'

const makeApiRequest = async() => {
  const apiResponse = await fetch('https://testwithfirebaseandreact.firebaseio.com/restaurants.json')
  return await apiResponse.json()
}

export default makeApiRequest;