import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';

import {
  changeToMap,
  searchByName,
  filterByRange,
  loadAllRestaurants,
} from '../actions/actions'



export default compose(
  connect ((state) => {
      return {
        searchedRestaurants: state.searchedRestaurants,
        searchedName: state.searchedName,
        restaurants: state.restaurants,
        showOnMap: state.showOnMap,
        range: state.range
      }
    },
    (dispatch) => ({
      loadAllRestaurants() {
        dispatch(loadAllRestaurants())
      },
      searchByName(e) {
        dispatch(searchByName(e.target.value))
      },
      filterByRange(e) {
        dispatch(filterByRange(e.target.value))
      },
      changeToMap(){
        dispatch(changeToMap())
      }
    })
  ),
  lifecycle({
    componentDidMount(){
     const { loadAllRestaurants }  = this.props;
     loadAllRestaurants();
    }
  })
)