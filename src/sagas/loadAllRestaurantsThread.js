import { call, put, takeEvery, takeLatest, select } from 'redux-saga/effects';
import _ from 'lodash';

import { LOAD_ALL_RESTAURANTS } from "../actions/actions";
import makeApiRequest from '../utils/apiRequest';


export function* loadAllRestaurants() {
  try {
    const items = yield makeApiRequest();
    yield put({type: 'FETCHED_RESTAURANTS', payload: items});
  }
  catch(error){
    console.log(error)
  }
}

function* loadAllRestaurantsThread() {
  yield takeLatest('LOAD_ALL_RESTAURANTS', loadAllRestaurants);
}

export default loadAllRestaurantsThread;

