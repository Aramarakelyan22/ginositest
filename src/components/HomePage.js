import React from 'react';
import RestaurantsList from './RestaurantsList';
import homePageEnhancer from '../containers/homePageEnhancer';
import SimpleMap from './MapComponent';
import CommonHeading from './CommonHeading';



function HomePage(props){

  const {
    range,
    showOnMap,
    changeToMap,
    restaurants,
    searchedName,
    searchByName,
    filterByRange,
    searchedRestaurants,
  } = props;

 return (
  <div >
    <CommonHeading
      range={range}
      showOnMap={showOnMap}
      changeToMap={changeToMap}
      searchByName={searchByName}
      searchedName={searchedName}
      filterByRange={filterByRange}
    />
    {showOnMap ?
      <SimpleMap restaurants={restaurants} searchedName={searchedName} searchedRestaurants={searchedRestaurants}/>:
      <RestaurantsList restaurants={restaurants} searchedName={searchedName} searchedRestaurants={searchedRestaurants}/>
    }
  </div>
  )
}


const EnhancedHomePage = homePageEnhancer(HomePage);
export default EnhancedHomePage

