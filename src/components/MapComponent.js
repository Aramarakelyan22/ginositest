import React from "react"
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const SimpleMap = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100vh` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) => {

  return (<GoogleMap
    defaultZoom={13.5}
    defaultCenter={{lat: 40.73, lng: -73.93}}
  >
    {props.searchedName.length === 0 && props.restaurants ? props.restaurants.map(rest => {
      return (<Marker key={rest.id} position={{lat: Number(rest.location.latitude), lng: Number(rest.location.longitude)}} label={rest.name} />)
      }
    ):
      props.searchedRestaurants && props.searchedRestaurants.map(rest => {
          return (<Marker key={rest.id} position={{lat: Number(rest.location.latitude), lng: Number(rest.location.longitude)}} label={rest.name} />)
        }
      )
    }
  </GoogleMap>)
})

export default SimpleMap
