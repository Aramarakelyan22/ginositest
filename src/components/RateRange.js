import React, { Component } from 'react'
import Slider from 'react-rangeslider'
/*import 'react-rangeslider/lib/index.css';*/

function RateRange(props) {

  return (
    <Slider
      value={5}
      orientation="vertical"
      onChange={() => {this.handleOnChange()}}
    />
  )
}

export default RateRange