import React from 'react';
export function RestaurantItem (props) {
const styles = {
  containerStyle: {
    textAlign: 'center',
    maxWidth: '300px',
    width: '100%',
    border: '1px solid',
    margin: '2px'
  }
}

  const {
    id,
    name,
    thumb,
    location,
    user_rating
  }  = props;

  return(
    <div
      key={id}
      style={styles.containerStyle}
    >
      <h2>{name}</h2>
      {thumb ? <img src={thumb} alt=""/> : <p>Without Image</p> }
      <p>{`Rating ${user_rating}`}</p>
      <address>{`Address ${location.address}`}</address>
    </div>
  )
}

export default RestaurantItem