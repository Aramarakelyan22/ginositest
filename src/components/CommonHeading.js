import React from 'react';
import Switch from 'react-switch';

function CommonHeading(props) {

  const {
    range,
    showOnMap,
    changeToMap,
    searchByName,
    searchedName,
    filterByRange
  } = props;

  return (
    <div style={{textAlign: 'center'}} >
      <h1>The Restaurants of The New York</h1>
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-around'}}>
        <div>
          <label htmlFor="search">
            <span>Search by Name</span>
            <input
              type="text"
              onChange={(e) => {searchByName(e)}}
              id="search"
            />
          </label>
          {searchedName.length !== 0 ?
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <span>Filter by rating</span>
              <input
                type='range'
                step='0.1'
                onChange={(e) => filterByRange(e)}
                max={5}
                min={4}
                defaultValue='0'
              />
              <span>({range})</span>
            </div>:
            null
          }
        </div>
        <div style={{display: 'flex', alignItems: 'center'}}>
          <span>Switch to map</span>
          <Switch
            label=""
            onChange={() => {changeToMap()}}
            checked={showOnMap}
            id="normal-switch"
          />
        </div>
      </div>
    </div>
  )
}
export default CommonHeading;