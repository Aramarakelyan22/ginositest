import React from 'react';
import StarRatingComponent from 'react-star-rating-component';

export function RatingStar(props) {
  const {
    rating,

  } = props;

  return (
    <div>
      <h2>Rating from state: {rating}</h2>
      <StarRatingComponent
        name="rate2"
        editing={false}
        renderStarIcon={() => <span></span>}
        starCount={5}
        value={Number(rating)}
      />
    </div>
  );
}


export default RatingStar