import React from 'react';

import RestaurantItem from './RestaurantItem';

function RestaurantsList(props) {

  const {
    restaurants,
    searchedName,
    searchedRestaurants,
  } = props;

  return (
    <div style={{display: "flex", flexWrap: 'wrap', justifyContent: 'center'}}>

      { searchedName.length === 0 ? restaurants && restaurants.map(rest => {
        return (
          <RestaurantItem
            key={rest.id}
            name={rest.name}
            thumb={rest.thumb}
            location={rest.location}
            user_rating = {rest.user_rating}
          />
        )
      }) :
        searchedRestaurants && searchedRestaurants.map(rest => {
          return (
            <RestaurantItem
              key={rest.id}
              name={rest.name}
              thumb={rest.thumb}
              location={rest.location}
              user_rating = {rest.user_rating}
            />
          )
        })
      }
    </div>
  )
}

export default RestaurantsList