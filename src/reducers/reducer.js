import {
  CHANGE_TO_MAP,
  SEARCH_BY_NAME,
  FILTER_BY_RANGE
} from '../actions/actions'

import { data } from '../utils/temporaryData'

const initialState = {
  searchedRestaurants:  [],
  searchedName: '',
  restaurants: [],
  showOnMap: false,
  range: 0
}

function homePageReducer(state = initialState, action) {
  let newState = null;
  switch(action.type) {
    case 'FETCHED_RESTAURANTS':
      newState = Object.assign({}, state);
      newState.restaurants = action.payload;
      return newState;
    case SEARCH_BY_NAME:
      let searchedRestaurants = [];
      newState = Object.assign({}, state);
      if( action.value.trim().length !== 0) {
        searchedRestaurants = state.restaurants.filter(rest => {
          return rest.name.indexOf(action.value) !== -1
        })
      }
      newState.searchedRestaurants = action.value.length !== 0 ? searchedRestaurants : initialState.restaurants;
      newState.searchedName = action.value;
      return newState;
    case FILTER_BY_RANGE:
      let filteredRestaurants = [];
      newState = Object.assign({}, state);
      newState.range = action.value;
        filteredRestaurants = state.restaurants.filter(rest => {
          if(state.searchedName.trim().length !== 0){
            return (Number(rest.user_rating)) >= Number(action.value) && rest.name.indexOf(state.searchedName) !== -1
          }
      });
      newState.searchedRestaurants = filteredRestaurants;
      return newState;
    case CHANGE_TO_MAP:
      newState = Object.assign({}, state);
      newState.showOnMap = !state.showOnMap;
      return newState;
    default:
      return state;
  }
}

export default homePageReducer;