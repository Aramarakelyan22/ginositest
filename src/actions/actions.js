//the function for loading the all restaurants
export const LOAD_ALL_RESTAURANTS = 'LOAD_ALL_RESTAURANTS';
export const loadAllRestaurants = () => {
  return {
    type: LOAD_ALL_RESTAURANTS
  }
}


export const SEARCH_BY_NAME = 'SEARCH_BY_NAME';
export function searchByName(value) {
  return {
    type: SEARCH_BY_NAME,
    value
  }
}

export const FILTER_BY_RANGE = 'FILTER_BY_RANGE';
export function filterByRange(value) {
  return {
    type: FILTER_BY_RANGE,
    value
  }
}


export const CHANGE_TO_MAP = 'CHANGE_TO_MAP';
export function changeToMap() {
  return {
    type: CHANGE_TO_MAP
  }
}