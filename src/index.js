import * as React from "react";
import  ReactDOM from "react-dom";
import  App  from "./App.jsx";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import reducer from './reducers/reducer';
import 'regenerator-runtime/runtime';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import loadAllRestaurants from './sagas/loadAllRestaurantsThread';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(function*() {
  yield [
    loadAllRestaurants(),
  ]
  });

if (process.env.NODE_ENV !== 'production') {
     console.log('Looks like we are in development mode!');
   }


   //Add Provider
ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider>
      <App />
    </MuiThemeProvider>
  </Provider>,
   document.getElementById("root")
);